<?php
/**
 * Title: Search page block pattern
 * Slug: hsbc_blocktheme/search
 * Categories: featured
 * inserter: no
 *
 * @package hsbc_blocktheme
 * @since 1.0.0
 */

?>
<!-- wp:heading {"className":"is-style-hsbc_blocktheme-text-shadow","fontSize":"x-large"} -->
<h2 class="is-style-hsbc_blocktheme-text-shadow has-x-large-font-size">
<?php echo hsbc_blocktheme_search_title(); ?></h2><!-- /wp:heading -->

