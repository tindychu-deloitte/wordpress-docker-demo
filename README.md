# WordPress docker demo
Dockerize wordpress

### Preparations
Install docker docker-compose, ref:
https://docs.docker.com/engine/install/ubuntu/
   
Install make:
```
sudo apt update && sudo apt install -y make
```

## Help
Run:
```
make help
```


## Local
Run:
```
make local
```

Website:
```
http://local:8080
```

DB adminer:
```
http://local:8001
```
server: db, username: admin, password: pw, database: local_db


## Production
1) Run:
```
chmod +x ./script/*.sh
```

2) update ./script/env.sh
   - DB_USER
   - DB_PASSWORD

3) . ./script/env.sh && rm -f ./script/env.sh

4) . ./script/set_cron.sh

Run as Prod:
```
make prod
```