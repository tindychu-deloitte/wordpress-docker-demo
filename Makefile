stage = prod local local-m1
.PHONY: $(stage)

HELP_CMD = grep -E '.*?\#\# .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?\#\# "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

$(stage): ## Run docker compose with different stages is [prod||local], d=1 as detached mode
	@if [ "$(@)" = "prod" ] && [ "$(DB_USER)" = "" ]; then echo "Missing DB_USER" && exit 1; fi
	@if [ "$(@)" = "prod" ] && [ "$(DB_PASSWORD)" = "" ]; then echo "Missing DB_PASSWORD" && exit 1; fi
	@if [ "$(d)" = "1" ]; then docker-compose -f docker-compose.$@.yml up -d; else docker-compose -f docker-compose.$@.yml up; fi

owner: ## Fixing unable to use plugin and theme problems (chown to www-data), r=1 as chown to self
	@if [ "$(r)" = "1" ]; then sudo chown -R $$(whoami) wp; else sudo chown -R www-data wp; fi
	
rm_local_db: ## Clear local db
	@sudo rm -rf db/local

run_adminer: ## ***risk*** Start adminer, s=[prod||local] ***risk***
	@if  [ "$(s)" = "" ]; then echo "missing s, s=stage" && exit 1; fi
	@docker-compose -f docker-compose.$(s).yml up -d adminer

stop_adminer: ## Stop adminer for security
	@. ./script/stop_adminer.sh

help:
	@${HELP_CMD}
