<?php
/**
 * Block styles.
 *
 * @package hsbc_blocktheme
 * @since 1.0.0
 */

/**
 * Register block styles
 *
 * @since 1.0.0
 *
 * @return void
 */
function hsbc_blocktheme_register_block_styles() {

	register_block_style( // phpcs:ignore WPThemeReview.PluginTerritory.ForbiddenFunctions.editor_blocks_register_block_style
		'core/template-part',
		array(
			'name'  => 'hsbc_blocktheme-sticky',
			'label' => __( 'Sticky header', 'hsbc_blocktheme' ),
		)
	);

	register_block_style( // phpcs:ignore WPThemeReview.PluginTerritory.ForbiddenFunctions.editor_blocks_register_block_style
		'core/button',
		array(
			'name'  => 'hsbc_blocktheme-flat-button',
			'label' => __( 'Flat button', 'hsbc_blocktheme' ),
		)
	);

	register_block_style( // phpcs:ignore WPThemeReview.PluginTerritory.ForbiddenFunctions.editor_blocks_register_block_style
		'core/button',
		array(
			'name'  => 'hsbc_blocktheme-button-shadow',
			'label' => __( 'Button with shadow', 'hsbc_blocktheme' ),
		)
	);

	register_block_style( // phpcs:ignore WPThemeReview.PluginTerritory.ForbiddenFunctions.editor_blocks_register_block_style
		'core/navigation',
		array(
			'name'  => 'hsbc_blocktheme-navigation-button',
			'label' => __( 'Button style', 'hsbc_blocktheme' ),
		)
	);

	register_block_style( // phpcs:ignore WPThemeReview.PluginTerritory.ForbiddenFunctions.editor_blocks_register_block_style
		'core/navigation',
		array(
			'name'  => 'hsbc_blocktheme-navigation-button-shadow',
			'label' => __( 'Button with shadow', 'hsbc_blocktheme' ),
		)
	);

	register_block_style( // phpcs:ignore WPThemeReview.PluginTerritory.ForbiddenFunctions.editor_blocks_register_block_style
		'core/list',
		array(
			'name'  => 'hsbc_blocktheme-list-underline',
			'label' => __( 'Underlined list items', 'hsbc_blocktheme' ),
		)
	);

	register_block_style( // phpcs:ignore WPThemeReview.PluginTerritory.ForbiddenFunctions.editor_blocks_register_block_style
		'core/group',
		array(
			'name'  => 'hsbc_blocktheme-box-shadow',
			'label' => __( 'Box shadow', 'hsbc_blocktheme' ),
		)
	);

	register_block_style( // phpcs:ignore WPThemeReview.PluginTerritory.ForbiddenFunctions.editor_blocks_register_block_style
		'core/column',
		array(
			'name'  => 'hsbc_blocktheme-box-shadow',
			'label' => __( 'Box shadow', 'hsbc_blocktheme' ),
		)
	);

	register_block_style( // phpcs:ignore WPThemeReview.PluginTerritory.ForbiddenFunctions.editor_blocks_register_block_style
		'core/columns',
		array(
			'name'  => 'hsbc_blocktheme-box-shadow',
			'label' => __( 'Box shadow', 'hsbc_blocktheme' ),
		)
	);

	register_block_style( // phpcs:ignore WPThemeReview.PluginTerritory.ForbiddenFunctions.editor_blocks_register_block_style
		'core/site-title',
		array(
			'name'  => 'hsbc_blocktheme-text-shadow',
			'label' => __( 'Text shadow', 'hsbc_blocktheme' ),
		)
	);

	register_block_style( // phpcs:ignore WPThemeReview.PluginTerritory.ForbiddenFunctions.editor_blocks_register_block_style
		'core/post-title',
		array(
			'name'  => 'hsbc_blocktheme-text-shadow',
			'label' => __( 'Text shadow', 'hsbc_blocktheme' ),
		)
	);

	register_block_style( // phpcs:ignore WPThemeReview.PluginTerritory.ForbiddenFunctions.editor_blocks_register_block_style
		'core/heading',
		array(
			'name'  => 'hsbc_blocktheme-text-shadow',
			'label' => __( 'Text shadow', 'hsbc_blocktheme' ),
		)
	);
}
add_action( 'init', 'hsbc_blocktheme_register_block_styles' );

/**
 * This is an example of how to unregister a core block style.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/block-api/block-styles/
 * @see https://github.com/WordPress/gutenberg/pull/37580
 *
 * @since 1.0.0
 *
 * @return void
 */
function hsbc_blocktheme_unregister_block_style() {
	wp_enqueue_script(
		'hsbc_blocktheme-unregister',
		get_stylesheet_directory_uri() . '/assets/js/unregister.js',
		array( 'wp-blocks', 'wp-dom-ready', 'wp-edit-post' ),
		HSBC_BLOCKTHEME_VERSION,
		true
	);
}
add_action( 'enqueue_block_editor_assets', 'hsbc_blocktheme_unregister_block_style' );
