<?php
/**
 * Registers the `gallery` block on server.
 */

function gutenify_register_block_advance_slide_item() {
	// $handle = 'gutenify-block-advance-slider';
	// $asset = include_once sprintf( '%sdist/' .$handle . '.asset.php', GUTENIFY_PLUGIN_DIR );

	// wp_register_script( $handle, GUTENIFY_PLUGIN_URL . 'dist/' . $handle . '.js', $asset['dependencies'], $asset['version'], false );
	// wp_register_style( $handle. '-style', GUTENIFY_PLUGIN_URL . 'dist/' . $handle . '.css', array(), $asset['version'] );
	// wp_register_style( 'style-' . $handle, GUTENIFY_PLUGIN_URL . 'dist/' . 'style-' . $handle . '.css', array(), $asset['version'] );
	// Return early if this function does not exist.
	if ( ! function_exists( 'register_block_type' ) ) {
		return;
	}

	register_block_type( GUTENIFY_PLUGIN_DIR . 'dist/blocks/advance-slide-item' );
}
add_action( 'init', 'gutenify_register_block_advance_slide_item' );
